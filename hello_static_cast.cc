#include <iostream>
#include <string>

using namespace std;

int main()

{

    cout << static_cast<string>("Hello, world!") << endl;

}

// C++ Casting, or: "Oh No, They Broke Malloc!" 2017-2018
// https://embeddedartistry.com/blog/2017/2/28/c-casting-or-oh-no-we-broke-malloc
